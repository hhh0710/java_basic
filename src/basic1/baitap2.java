package basic1;
import java.util.Scanner;
public class baitap2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("nhap n");
        int n = sc.nextInt();
        sc.close();
        int result = 0;
        int i =0;
        // tính tổng n số nguyên tố đầu tiên
        for (int j=0;;j++){
            if(isprime(j)){
                result +=j;
                i++;
            }
            if(i==n) break;
        }
        System.out.println(result);
    }

    // hàm kiểm tra số nguyên tố
    public static boolean isprime(int n) {
        if (n <= 1){
            return false;
        }
        for(int i = 2;i<= Math.sqrt(n);i++){
            if (n % i==0) {
                return false;
            }
        }
        return true;
    }
}

