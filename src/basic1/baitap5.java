package basic1;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class baitap5 {
    public static void main(String[] args) throws IOException {

//        if (file.isFile()) {
//            System.out.println("File is exist" + file.getAbsolutePath()); // get link tuyệt đối;
//        } else {
//            System.out.println("File doesn't exist");
//            try {
//                file.createNewFile();   // tạo mới 1 file   // khi dùng hàm này sẽ phải có throws IOException ở main không thì phải dùng try catch
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        FileInputStream fileinput = null;
//        FileOutputStream fileoutput = null;
//        try {
//            fileinput = new FileInputStream(fileurl);
//            fileoutput = new FileOutputStream("output.txt");
//            // đọc dữ liệu trong 1 file
//            int ch;
//            while ((ch = fileinput.read()) != -1) {
//                System.out.print((char)ch);
//                //ghi dữ liệu vào output.txt
//                fileoutput.write(ch);
//            }
//        }
////        catch (FileNotFoundException e) {
////            e.printStackTrace();
////        }
////        catch (IOException e) {
////            e.printStackTrace();
////        }
//        // nếu dùng catch (Exception e) thì không cần dùng 2 catch trên.
//        catch (Exception e) {
//            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
//        }
//        finally {
//            if (fileinput != null) {
//                try {
//                    fileinput.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


        // dùng FileReader và FileWriter (note: chỉ dùng với file text)
//        FileReader reader = null;
//        FileWriter writer = null;
//        try {
//            reader = new FileReader(fileurl);
//            writer = new FileWriter("output2.txt");
//            ArrayList<Character> arr = new ArrayList();
//            int ch2 ;
//            while ((ch2 = reader.read()) != -1){
//                System.out.print((char)ch2);
//                arr.add((char)ch2);
//                writer.write(ch2);
//            }
//            String seach = "vis tranning";
//            for (int i =0;i <arr.size();i++){
//                if (arr.get(i) == -1){
//                    writer.write(String.valueOf(arr));
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


//        FileInputStream fileInputStream = null;
//        BufferedInputStream bufferInputStream = null;
//        try{
//            fileInputStream = new FileInputStream(fileurl);
//            bufferInputStream = new BufferedInputStream(fileInputStream);
//
//        }catch(Exception e){
//
//        }finally{
//            if(fileInputStream != null){
//                try {
//                    fileInputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(bufferInputStream != null){
//                try {
//                    bufferInputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


//        Scanner sc = new Scanner(System.in);
//        // Nhập đường dẫn đến file
//        String fileurl = sc.nextLine();
//        sc.close();
//        File file = new File(fileurl);

//        FileInputStream fis = null;
//        BufferedReader br = null;
//        FileOutputStream fos = null;
//        DataOutputStream dos = null;
//        try {
//            fis = new FileInputStream(file);
//            br = new BufferedReader(new InputStreamReader(fis));
//            fos = new FileOutputStream("output.txt");
//            dos = new DataOutputStream(fos);
//
//            ArrayList<String> arrayList = new ArrayList();
//            String read = br.readLine();
//            while (read != null){
//                System.out.println(read);
//                arrayList.add(read);
//                read = br.readLine();
//            }
//            String seach = "vis tranning";
//            for (int i =0;i <arrayList.size();i++){
//                if (arrayList.get(i).toLowerCase().indexOf(seach) != -1){
//                    dos.writeChars(arrayList.get(i) + "\n");
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally{
//            dos.close();
//            fos.close();
//            br.close();
//            fis.close();
//        }
        System.out.println("nhap vao duong dan file:");
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        File file = new File(fileName);
//       FileInputStream fin = null;
//        BufferedInputStream bis = null;vis traning
//        fin = new FileInputStream(file);
//        BufferedReader br = new BufferedReader(fin);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        FileWriter fw = new FileWriter("write_file.txt");
        String i="";
        while ((i = br.readLine()) != null) {
            if(i.toLowerCase().contains("vis training")){
                System.out.println(i);
                fw.write(i + "\n");
            }
        }
        fw.close();
        br.close();
        fr.close();


    }
}
