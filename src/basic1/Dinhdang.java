package basic1;

import java.io.Serializable;

public class Dinhdang implements Serializable {
    public static final long serialVersionUID = 1L;
    private String sdt;
    private String nhamang;
    private String sotien;
    public Dinhdang() {}
    public Dinhdang(String sdt, String nhamang, String sotien){
        this.sdt = sdt;
        this.nhamang = nhamang;
        this.sotien = sotien;
    }


    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getNhamang() {
        return nhamang;
    }

    public void setNhamang(String nhamang) {
        this.nhamang = nhamang;
    }

    public String getSotien() {
        return sotien;
    }

    public void setSotien(String sotien) {
        this.sotien = sotien;
    }
    @Override
    public String toString() {
        return "[So dien thoai: " + sdt + ", Nha mang: " + nhamang + ", So tien: " + sotien + "]";
    }
}
